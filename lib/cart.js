const cartBody = document.querySelector('.cart-items')
const list = cartBody.querySelector('ul')

// cartBody.style.backgroundColor = 'salmon'

const storedItems = JSON.parse( localStorage.getItem('cart'))


 

  
  storedItems.map(cartItem => {
    // console.log(cartItem);
    // Creating Elements
    const li = document.createElement('li')
    const img = document.createElement('img')
    const h2 = document.createElement('h2')
    const p = document.createElement('p')
    const small = document.createElement('small')
    const span = document.createElement('span')
    span.className = "delete"
    const namePrice = document.createElement('div')

    // Setting attributes and Value
    namePrice.setAttribute('class', "name-price")
    img.setAttribute('src', "../assets/" + cartItem.imgUrl)
    h2.textContent = cartItem.itemName
    p.textContent = cartItem.itemPrice
    span.textContent = "Delete"

    // Appending to DOM
    namePrice.appendChild(h2)
    namePrice.appendChild(p)
    li.appendChild(img)
    li.appendChild(namePrice)
    li.appendChild(span)
    list.appendChild(li)

  })

  list.addEventListener('click', (e) => {
    if(e.target.className === 'delete'){
      const deleteList = e.target.parentElement 
      const itemParent = e.target.parentElement.querySelector('.name-price')
      const itemName = itemParent.querySelector('h2').textContent
      list.removeChild(deleteList) 
      
      for (let index = 0; index < storedItems.length; index++) {
        const element = storedItems[index];
        console.log(element);
        if (element.itemName === itemName) {
          storedItems.splice(index, 1)
        }
        
      }
     localStorage.setItem('cart', JSON.stringify(storedItems))  
  }
  })


// const listItems = document.getElementsByTagName('main')
// console.log(listItems);
const searchItem = document.forms[0]
const listItems = document.querySelector('.cart-items')
// console.log(listItems);
searchItem.addEventListener('keyup', function(e){
  const word = e.target.value.toLowerCase()

  const pictures = listItems.getElementsByTagName('h2')
  // console.log(pictures)
  Array.from(pictures).forEach(function(item){
    const title = item.textContent
    // console.log(title);
    if(title.toLowerCase().indexOf(word) != -1){
    item.parentElement.parentElement.style.display ='flex'
  } else {
    item.parentElement.parentElement.style.display = "none"
  }
  })
  
})